<?php

namespace Drupal\pixel_socketio\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Process;

require_once drupal_get_path('module', 'pixel_socketio') . '/vendor/autoload.php';

/**
 * Configure pixel_socketio settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Server path variable.
   *
   * @var string
   */
  private $server;

  /**
   * Variable for CacheBackendInterface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Variable for State.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Variable for Database Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Variable for EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pixel_socketio_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pixel_socketio.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = new static();
    $form->cacheBackend = $container->get('cache.default');
    $form->state = $container->get('state');
    $form->database = $container->get('database');
    $form->entityTypeManager = $container->get('entity_type.manager');
    return $form;
  }

  /**
   * For server path initialization.
   */
  public function __construct() {
    $this->server = drupal_get_path('module', 'pixel_socketio') . "/src/Server.php";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($cache = $this->cacheBackend->get($this->getFormId() . "_status")) {
      $status = $cache->data;
    }
    else {
      $serverStatus = new Process(['php', $this->server, 'status']);
      $serverStatus->run();

      $status = "Latest Fetch: " . date("Y-m-d h:i:s") . "\n" . preg_replace('/^.+\n/', '', $serverStatus->getOutput());

      $this->cacheBackend->set($this->getFormId() . "_status", $status, time() + 600);
    }

    $form['pixel_socketio'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['action'] = [
      '#type' => 'details',
      '#title' => 'Actions',
      '#group' => 'pixel_socketio',
    ];

    $form['action']['start'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start'),
      '#submit' => ['::start'],
      '#attributes' => [
        'style' => ['width: 100%;margin: 0 0 10px 0;'],
      ],
    ];

    $form['action']['stop'] = [
      '#type' => 'submit',
      '#value' => $this->t('Stop'),
      '#submit' => ['::stop'],
      '#attributes' => [
        'style' => ['width: 100%;margin: 0 0 10px 0;'],
      ],
    ];

    $form['action']['restart'] = [
      '#type' => 'submit',
      '#value' => $this->t('Restart'),
      '#submit' => ['::restart'],
      '#attributes' => [
        'style' => ['width: 100%;margin: 0 0 10px 0;'],
      ],
    ];

    $form['status'] = [
      '#type' => 'details',
      '#title' => 'Status',
      '#group' => 'pixel_socketio',
    ];

    $form['status']['refresh'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh Status'),
      '#attributes' => [
        'style' => ['width: 100%;margin: 0 0 10px 0;'],
      ],
    ];

    $form['status']['info'] = [
      '#markup' => '<pre>' . $status . '</pre>',
      '#attributes' => [
        'style' => [
          "line-height: 16px;",
          "background: linear-gradient(180deg,#ccc 0,#ccc 1.5em,#eee 0);",
          "background-size: 3em 3em;",
          "background-origin: content-box;",
          "padding: 0px 20px;",
          "text-align: justify;",
          "font-family: monospace;",
          "font-weight: bold;",
          "text-align: center;",
        ],
      ],
    ];

    $form['log'] = [
      '#type' => 'details',
      '#title' => 'Logs',
      '#group' => 'pixel_socketio',
    ];

    $header = [
      ['data' => $this->t('Date'), 'field' => 'timestamp'],
      ['data' => $this->t('User'), 'field' => 'uid'],
      ['data' => $this->t('Message'), 'field' => 'message'],
    ];

    /**
     * @var \Drupal\Core\Database\Query\SelectInterface
     */
    $query = $this->database->select('watchdog', 'wd');
    $query->fields('wd', ['timestamp', 'uid', 'message']);
    $query->condition("type", "pixel_socketio");
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $result = $pager->execute();
    $users = array_reduce($this->entityTypeManager->getStorage("user")->loadMultiple(), function ($carry, User $item) {
      $carry[$item->id()] = $item->getAccountName();
      return $carry;
    }, []);

    $rows = [];

    foreach ($result as $row) {
      $row->uid = $users[$row->uid];
      $row->timestamp = date("Y.m.d H:i:s", $row->timestamp);
      $rows[] = ['data' => (array) $row];
    }

    $form['log']['detail'] = [
      '#markup' => $this->t('<p>List of All Logs related this module</p>'),
    ];

    $form['log']['detail']['location_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
    $form['log']['detail']['pager'] = [
      '#type' => 'pager',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit handler to start pixel_socketio server.
   */
  public function start($form, &$form_state) {
    $process = Process::fromShellCommandline("php $this->server start -d");
    $process->start();
    $process->wait();

    if (substr(trim($process->getOutput()), -15) !== "already running") {
      $this->state->set("pixel_socketio_status", TRUE);
      $this->messenger()->addMessage($this->t('The server has been started.'));
      $this->logger("pixel_socketio")->info($this->t('The server has been started.'));
      $this->refresh($form, $form_state);
      drupal_flush_all_caches();
      $this->messenger()->addMessage($this->t('Caches have been removed.'));
      $this->logger("pixel_socketio")->info($this->t('Caches have been removed.'));
    }
    else {
      $this->messenger()->addWarning($this->t('The server was already started.'));
      $this->logger("pixel_socketio")->info($this->t('The server was already started.'));
    }
  }

  /**
   * Submit handler to stop pixel_socketio server.
   */
  public function stop($form, &$form_state) {
    $serverStatus = new Process(['php', $this->server, 'stop']);
    $serverStatus->run();

    $status = trim(substr($serverStatus->getOutput(), -8));
    if ($status === "not run") {
      $this->messenger()->addWarning($this->t("The server was not started therefore can't stop"));
      $this->logger("pixel_socketio")->info($this->t("The server was not started therefore can't stop"));
    }
    else {
      $this->state->set("pixel_socketio_status", FALSE);
      $this->messenger()->addStatus($this->t("The server has been stopped."));
      $this->logger("pixel_socketio")->info($this->t("The server has been stopped."));
      $this->refresh($form, $form_state);
    }
  }

  /**
   * Submit handler to restart pixel_socketio server.
   */
  public function restart($form, &$form_state) {
    $this->stop($form, $form_state);
    $this->start($form, $form_state);
  }

  /**
   * Submit handler to refresh pixel_socketio server status.
   */
  public function refresh($form, &$form_state) {
    $this->cacheBackend->delete($this->getFormId() . "_status");
    $this->messenger()->addMessage($this->t('Status refreshed.'));
    $this->logger("pixel_socketio")->info($this->t("Status refreshed."));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('pixel_socketio.settings')
      ->set('example', $form_state->getValue('example'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
