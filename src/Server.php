<?php

namespace Drupal\pixel_socketio;

use Workerman\Worker;
use PHPSocketIO\SocketIO;

require_once dirname(__DIR__) . '/vendor/autoload.php';

/**
 * Server class for pixel_socketio.
 */
class Server {

  /**
   * Server port.
   *
   * @var int
   */
  private $port = 3000;

  /**
   * Function for emitting messages.
   */
  public function __construct() {
    $io = new SocketIO($this->port);
    $io->on('connection', function ($socket) use ($io) {
      $socket->on('pixel_socketio', function ($msg) use ($io) {
        $io->emit('pixel_socketio', $msg);
      });
    });
    Worker::runAll();
  }

}

new Server();
