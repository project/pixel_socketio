jQuery(function () {
  var socket = io('http://' + document.domain + ':3000');

  jQuery('table tr td').click(function (e) {
    e.preventDefault(); // prevents page reloading
    console.log(jQuery(this).parent().index() * 10 + jQuery(this).index() + 1);
    console.log(socket.emit('pixel_socketio', jQuery(this).parent().index() * 10 + jQuery(this).index()));
    return false;
  });

  socket.on('pixel_socketio', (data) => {
    if (!isNaN(data))
      jQuery("table tr:nth-child(" + (parseInt(data / 10) + 1) + ") td:nth-child(" + (data % 10 + 1) +")").css("background", "red");
  });
});
